#BOF


LIBS := list-comprehension-demos scad-utils threadlib IoP-satellite

RSYNC_OPTIONS := --archive --verbose --exclude ".git" --delete --delete-excluded --omit-dir-times --checksum

.PHONY: ${LIBS}

all : ${LIBS}

clean : ${addsuffix .clean,${LIBS}}




threadlib : 
	[ -d "$@" ] || git clone https://github.com/adrianschlatter/threadlib.git "$@"
	(cd "$@" ; git pull)
	rsync ${RSYNC_OPTIONS} "$@/" "../$@/"

threadlib.clean :
	rm -rf ../threadlib threadlib




IoP-satellite : 
	[ -d "$@" ] || git clone https://github.com/MisterHW/IoP-satellite.git "$@"
	(cd "$@" ; git pull)
	rsync ${RSYNC_OPTIONS} "IoP-satellite/OpenSCAD bottle threads/thread_profile.scad" "../thread_profile.scad"

IoP-satellite.clean :
	rm -rf IoP-satellite ../thread_profile.scad




scad-utils :
	[ -d "$@" ] || git clone https://github.com/openscad/scad-utils.git "$@"
	(cd "$@" ; git pull)
	rsync ${RSYNC_OPTIONS} "$@/" "../$@/"

scad-utils.clean :
	rm -rf ../scad-utils scad-utils





list-comprehension-demos :
	[ -d "$@" ] || git clone https://github.com/openscad/list-comprehension-demos.git "$@"
	(cd "$@" ; git pull)
	rsync ${RSYNC_OPTIONS} "$@/" "../$@/"

list-comprehension-demos.clean :
	rm -rf ../list-comprehension-demos list-comprehension-demos




#EOF
