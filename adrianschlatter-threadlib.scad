include <threadlib/threadlib.scad>
include <threadlib/THREAD_TABLE.scad>


function thread_pitch(descriptor) = (
    THREAD_TABLE[search( [descriptor], THREAD_TABLE, num_returns_per_match = 1)[0]][1][0]
);	


function support_diameter(descriptor) = (
    THREAD_TABLE[search( [ descriptor], THREAD_TABLE, num_returns_per_match = 1)[0]][1][2]
);
